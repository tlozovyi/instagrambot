package com.tlozovyi.instagrambot;

import org.openqa.selenium.*;

import io.appium.java_client.android.*;

public class VideoPage extends DefaultPage {
    private static final By likeButton = byId("aah");
    private static final By profileButton = byId("ejp");
    private static final By numberOfLikesView = byId("aak");
    private static final By resumeButton = byXpath("//android.support.v4.view.ViewPager[@resource-id='" + RES_PREFIX + "eq5']/../following-sibling::android.widget.ImageView");

    public VideoPage(AndroidDriver driver) {
        super(driver);
    }

    public boolean isLikeButtonPresent() {
        return isPresent(likeButton);
    }

    public void clickLikeButton() {
        click(likeButton);
    }

    public void clickProfileButton() {
        click(profileButton);
    }

    public boolean isLikeClicked() {
        return isSelected(likeButton);
    }

    public int getNumberOfLikes() {
        String text = find(numberOfLikesView).getText();
        if (text.contains("K")) {
            System.out.println("Number of likes text: " + text);
            text = text.replace("K", "");
            return (int) (Double.parseDouble(text) * 1000) ;
        }
        return Integer.parseInt(text);
    }

    public boolean isResumeButtonVisible() {
        return isPresent(resumeButton);
    }

    public void likeVideoAndProceed() {
        if (!isResumeButtonVisible()) {
            clickScreenCenter();
        }

        if (!isLikeClicked()) {
            System.out.println("Liked video");
            clickLikeButton();
        }
        delayRandom(100);
        scroll(DefaultPage.DIRECTION_DOWN);
        delayRandom(200);
        clickScreenCenter();
    }
}
