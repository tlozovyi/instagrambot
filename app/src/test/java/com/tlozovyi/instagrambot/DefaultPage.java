package com.tlozovyi.instagrambot;

import com.google.common.collect.*;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.*;


import static com.tlozovyi.instagrambot.AdbController.sendText;

public abstract class DefaultPage {
    private final static By toolbarBackButton = byId("action_bar_button_back");

    public static final String PACKAGE = "com.zhiliaoapp.musically";
    public static final String RES_PREFIX = PACKAGE + ":id/";
    public static final String ANDROID_RES = "android:id/";

    public static final int DIRECTION_LEFT = 0;
    public static final int DIRECTION_UP = 1;
    public static final int DIRECTION_RIGHT = 2;
    public static final int DIRECTION_DOWN = 3;
    public static final int INVALID_VALUE = -1;

    public static final int DEFAULT_TIMEOUT = 5;

    //classes
    public static final String TEXT_VIEW = "android.widget.TextView";
    public static final String CHECKED_TEXT_VIEW = "android.widget.CheckedTextView";

    protected static By byId(String id) {
        return By.id(RES_PREFIX + id);
    }

    protected static By byAndroidId(String id) {
        return By.id(ANDROID_RES + id);
    }

    protected static By byClassAndText(String widgetClass, String text) {
        return By.xpath("//" + widgetClass + "[@text=\"" + text + "\"]");
    }

    protected static By byXpath(String xpath) {
        return By.xpath(xpath);
    }

    //buttons
    private static By positiveAndroidDialogButton = By.id(ANDROID_RES + "button1");
    private static By negativeAndroidDialogButton = By.id(ANDROID_RES + "button2");
    private static By neutralAndroidDialogButton = By.id(ANDROID_RES + "button3");

    private static By positiveInstagramDialogButton = By.id(RES_PREFIX + "button_positive");

    protected static By elementOfDialogList(String name) {
        return By.xpath("//android.widget.TextView[@resource-id='android:id/text1' and contains(@text,'" + name + "')]");
    }

    protected AndroidDriver driver;

    protected DefaultPage() {
    }

    protected DefaultPage(AndroidDriver driver) {
        this.driver = driver;
    }

    //dialogs
    public DefaultPage closeDialogWithNegativeButton() {
        writeLog("Closing dialog with negative button");
        if (isPresent(negativeAndroidDialogButton)) {
            driver.findElement(negativeAndroidDialogButton).click();
        }
        return this;
    }

    public DefaultPage closeDialogWithPositiveButton() {
//        writeLog("Closing dialog with positive button");
        if (isPresent(positiveInstagramDialogButton)) {
            driver.findElement(positiveInstagramDialogButton).click();
        } else if (isPresent(positiveAndroidDialogButton)) {
            driver.findElement(positiveAndroidDialogButton).click();
        }
        return this;
    }

    public DefaultPage closeDialogWithNeutralButton() {
        writeLog("Closing dialog with neutral button");
        if (isPresent(neutralAndroidDialogButton)) {
            driver.findElement(neutralAndroidDialogButton).click();
        }
        return this;
    }

    public boolean isBackButtonPresent() {
        return isPresent(toolbarBackButton);
    }

    public void clickBackButton() {
        click(toolbarBackButton);
    }


    public boolean isPresent(By locator) {
        List elements = driver.findElements(locator);
        return !elements.isEmpty();
    }

    protected void delay(int millis) {
        AdbController.delay(millis);
    }

    protected void delayRandom(int millis) {
        int delay = (int) (millis * (1 + new Random().nextDouble()));
        delay(delay);
    }

    protected WebElement waitFor(By locator) {
        return waitFor(locator, DEFAULT_TIMEOUT);
    }

    protected WebElement waitFor(By locator, int timeOutInSeconds) {
        return new WebDriverWait(driver, timeOutInSeconds)
            .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    protected WebElement findAndTypeIn(String name, String text, boolean forceClear) {
        scrollTo(name);
        return typeIn(By.name(name), text, forceClear);
    }

    protected WebElement typeIn(By locator, String text) {
        return typeIn(locator, text, false);
    }

    protected WebElement typeIn(By locator, String text, boolean forceClear) {
        WebElement element = driver.findElement(locator);
        element.click();
        if (forceClear) {
            forceClear(element);
        }
        sendText(text);
        return element;
    }

    protected WebElement findByName(String name) {
        By by = By.name(name);
        try {
            return driver.findElement(by);
        } catch (NoSuchElementException e) {
            hideKeyboard();
            scrollTo(name);
            return driver.findElement(by);
        }
    }

    public void scroll(int direction) {
        scroll(direction, INVALID_VALUE, INVALID_VALUE);
    }

    public void scroll(int direction, int x, int y) {
        scroll(direction, x, y, INVALID_VALUE, INVALID_VALUE);
    }

    public void scroll(int direction, int x, int y, int dx, int dy) {
        Dimension dimensions = driver.manage().window().getSize();
        int height = dimensions.getHeight();
        int width = dimensions.getWidth();

        double scrollStartX = width * 0.15 + new Random().nextDouble() * 0.1;
        double scrollEndX = width * 0.15 + new Random().nextDouble() * 0.1;
        double scrollStartY = height * 0.4 + new Random().nextDouble() * 0.1;
        double scrollEndY = height * 0.4 + new Random().nextDouble() * 0.1;

        int directionMultiplier = 1; // direction will define the sign of diff vars
        switch (direction) {
            case DIRECTION_LEFT:
                scrollEndX = width * 0.8 - new Random().nextDouble() * 0.1;
                break;

            case DIRECTION_UP:
                scrollEndY = height * 0.8 - new Random().nextDouble() * 0.1;
                break;

            case DIRECTION_RIGHT:
                scrollEndX = width * 0.01 + new Random().nextDouble() * 0.1;
                directionMultiplier = -1;
                break;

            case DIRECTION_DOWN:
                scrollEndY = height * 0.2 + new Random().nextDouble() * 0.1;
                directionMultiplier = -1;
                break;
        }
        int diffX = (int) (scrollEndX - scrollStartX);
        int diffY = (int) (scrollEndY - scrollStartY);
        if (dx != INVALID_VALUE) {
            diffX = directionMultiplier * dx;
        }
        if (dy != INVALID_VALUE) {
            diffY = directionMultiplier * dy;
        }
        int startX = (int) scrollStartX;
        int startY = (int) scrollStartY;
        if (x > INVALID_VALUE && y > INVALID_VALUE) {
            startX = x;
            startY = y;
        }
        int endX = Math.max(startX + diffX, 1);
        int endY = Math.max(startY + diffY, 1);
        TouchAction ts = new TouchAction(driver);
        ts.press(PointOption.point(startX, startY))
            .waitAction()
            .moveTo(PointOption.point(endX, endY))
            .release()
            .perform();
    }

    // TODO: 10/19/16 check that code below
    public void scrollTo(String name) {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0))." +
                "scrollIntoView(new UiSelector().textContains(\"" + name + "\").instance(0))");
    }

    public void forceClear(By locator) {
        forceClear(driver.findElement(locator));
    }

    public void forceClear(WebElement field) {
        field.clear();
    }

    public boolean isBelow(By locator, int y) {
        List<WebElement> elements = driver.findElements(locator);
        for (WebElement element : elements) {
            if (element.getLocation().y > y) {
                return true;
            }
        }
        return false;
    }

    public boolean isAbove(By locator, int y) {
        List<WebElement> elements = driver.findElements(locator);
        for (WebElement element : elements) {
            if (element.getLocation().y < y) {
                return true;
            }
        }
        return false;
    }

    public boolean isBetween(By locator, int startY, int endY) {
        List<WebElement> elements = driver.findElements(locator);
        for (WebElement element : elements) {
            int y = element.getLocation().y;
            if (y > startY && y < endY) {
                return true;
            }
        }
        return false;
    }

    public WebElement findBelow(By by) {
        try {
            return driver.findElement(by);
        } catch (NoSuchElementException ex) {
            hideKeyboard();
            // Scrolling down a few times, if still unable find - failing
            for (int i = 0; i < 5; ++i) {
                WebElement e = find(by);
                if (e != null) {
                    return e;
                }
                scroll(DIRECTION_DOWN);
            }
        }
        return driver.findElement(by);
    }

    public WebElement find(By by) {
//        try {
            return driver.findElement(by);
//        } catch (NoSuchElementException e) {
//            return null;
//        }
    }

    public boolean click(By by) {
        return click(find(by));
    }

    public boolean isSelected(By by) {
        return find(by).isSelected();
    }

    public boolean click(WebElement element) {
        if (element != null) {
            element.click();
        }
        return element != null;
    }

    public void clickScreenCenter() {
        Dimension dimensions = driver.manage().window().getSize();
        int height = dimensions.getHeight();
        int width = dimensions.getWidth();

        int offsetX = (int)(new Random().nextDouble() * 50 - 25);
        int offsetY = (int)(new Random().nextDouble() * 50 - 25);
        int startX = offsetX + width / 2;
        int startY = offsetY + height / 2;

        TouchAction ts = new TouchAction(driver);
        ts.tap(PointOption.point(startX, startY))
            .perform();
    }

    public boolean hideKeyboard() {
        writeLog("Trying to hide virtual keyboard");
        try {
            driver.hideKeyboard();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

//    public void longTap(WebElement element) {
//        new TouchAction(driver).longPress(element).release().perform();
//    }


    public DefaultPage goBack() {
        hideKeyboard();
        pressBackButton();
        delay(1);
        return this;
    }

    public DefaultPage pressBackButton() {
        writeLog("Pressing Back button");
        driver.navigate().back();
        return this;
    }

//    public DefaultPage pressEnterButton() {
//        writeLog("Pressing Enter button");
//        driver.pressKeyCode(AndroidKeyCode.ENTER);
//        return this;
//    }

    public void writeLog(String message) {
        AdbController.writeLog(message);
    }

    public void rotate(ScreenOrientation screenOrientation) {
        driver.rotate(screenOrientation);
    }

    protected void scrollToBottom(By locator) {
        String lastTitle = null;
        while (true) {
            List elements = driver.findElements(locator);
            String lastElementTitle;
            if (elements.isEmpty() || (lastElementTitle = ((WebElement) elements.get(elements.size() - 1)).getText()).equals(lastTitle)) {
                break;
            }
            scroll(DIRECTION_DOWN);
            lastTitle = lastElementTitle;
        }
    }
}
