package com.tlozovyi.instagrambot;

import org.openqa.selenium.By;

import io.appium.java_client.android.AndroidDriver;

public class SearchPage extends DefaultPage {
    private static final By searchButton = byXpath("//android.widget.EditText[@resource-id='" + RES_PREFIX + "action_bar_search_edit_text']");

    public SearchPage(AndroidDriver driver) {
        super(driver);
    }

    public void clickSearchButton() {
        writeLog("Click search button");
        click(searchButton);
        delay(3000);
    }

    public void clickTimeManagementTag() {
        writeLog("Click #timemanagement");
        click(byClassAndText(TEXT_VIEW, "#timemanagement"));
        delay(5000);
    }

    public void openFirstRecentPhoto() {
        writeLog("Open recent photos");
        scroll(DIRECTION_DOWN);
        scroll(DIRECTION_DOWN);
        scroll(DIRECTION_DOWN);
        delay(2000);
        click(byXpath("//android.widget.LinearLayout[@resource-id='" + RES_PREFIX + "media_set_row_content_identifier']/android.widget.ImageView"));
        delay(3000);
    }

//    public void typeInSearch(String query) {
//        clickSearchButton();
//        typeIn(searchButton, query);
//        pressEnterButton();
//        delay(3000);
//    }
}
