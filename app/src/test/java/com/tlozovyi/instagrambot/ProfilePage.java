package com.tlozovyi.instagrambot;

import org.openqa.selenium.*;

import java.util.*;

import io.appium.java_client.android.AndroidDriver;

public class ProfilePage extends DefaultPage {
    private static final By videoContainer = byId("a3i");

    public ProfilePage(AndroidDriver driver) {
        super(driver);
    }

    public int getNumberOfVideosOnScreen() {
        List videos = driver.findElements(videoContainer);
        return videos.size();
    }

    public void clickOnFirstVideo() {
        click(((List<WebElement>) driver.findElements(videoContainer)).get(0));
    }
}
