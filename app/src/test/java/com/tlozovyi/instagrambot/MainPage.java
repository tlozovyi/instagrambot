package com.tlozovyi.instagrambot;

import org.openqa.selenium.By;

import io.appium.java_client.android.AndroidDriver;

public class MainPage extends DefaultPage {
    private static final By likeButton = byXpath("//android.widget.ImageView[@resource-id='" + RES_PREFIX + "row_feed_button_like' and @content-desc='Like']");
    private static final By followButton = byXpath("//android.widget.TextView[@resource-id='" + RES_PREFIX + "button' and @content-desc='Follow']");
    private static final By likesTextView = byXpath("//android.widget.TextView[@resource-id='" + RES_PREFIX + "row_feed_textview_likes']");
    private static final By searchPageButton = byXpath("//*[@content-desc='Search and Explore']");
    private static final By profilePageButton = byXpath("//*[@content-desc='Profile']");

    public MainPage(AndroidDriver driver) {
        super(driver);
    }

    public boolean isLikeButtonPresent() {
        return isPresent(likeButton);
    }

    public void clickLikeButton() {
        click(likeButton);
    }

    public void clickSearchPage() {
        writeLog("Open search page");
        click(searchPageButton);
        delay(3000);
    }

    public void clickProfileButton() {
        writeLog("Open profile page");
        click(profilePageButton);
        delay(3000);
    }

    public boolean isLikesTextViewPresent() {
        return isPresent(likesTextView);
    }

    public void clickLikesTextView() {
        click(likesTextView);
    }

    public boolean isFollowButtonPresent() {
        return isPresent(followButton);
    }

    public void clickFollowButton() {
        click(followButton);
    }

}
