package com.tlozovyi.instagrambot;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Random;

public class Tests extends DefaultTest {
    private static final String MAIN_PACKAGE = "com.zhiliaoapp.musically";
    private static final String MAIN_ACTIVITY_NAME = "com.zhiliaoapp.musically/com.ss.android.ugc.aweme.splash.SplashActivity";

    public static final int MAX_VIDEOS_PER_PROFILE = 6;
    public static final int MAX_LIKES_TO_PROCESS_PROFILE = 75;

    private VideoPage videoPage;
    private ProfilePage profilePage;

    private MainPage mainPage;
    private SearchPage searchPage;

    protected static DesiredCapabilities getEmailCapabilities() {
        DesiredCapabilities capabilities = getBasicCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Android1");
        capabilities.setCapability("appPackage", "com.android.settings");
        capabilities.setCapability("appActivity", ".DisplaySettings");
        capabilities.setCapability("appWaitActivity", "com.android.settings.DisplaySettings");
        capabilities.setCapability("session-override", "true");
        capabilities.setCapability("debug-log-spacing", "true");
        capabilities.setCapability("noSign", "true");
        capabilities.setCapability("newCommandTimeout", "2000000");// 33.33 mins
        capabilities.setCapability("deviceReadyTimeout", "120000");
        capabilities.setCapability("platformVersion", AdbController.getOSVersion());
        return capabilities;
    }

    @BeforeClass
    public static void globalSetUp() throws Exception {
        startAppiumServer();
        setupDriver(getEmailCapabilities());
    }

    @AfterClass
    public static void globalTearDown() throws Exception {
        stopAppiumServer();
    }

    @Before
    public void setUp() throws Exception {
        videoPage = new VideoPage(driver);
        mainPage = new MainPage(driver);
        searchPage = new SearchPage(driver);
        profilePage = new ProfilePage(driver);
        AdbController.launchApp(MAIN_ACTIVITY_NAME);
        delay(5000);
    }

    @After
    public void tearDown() throws Exception {
        AdbController.closeApplication(MAIN_PACKAGE);
    }

    @Test
    public void test() {
        videoPage.clickScreenCenter();
        likeVideosOfNewAccounts(5);
        delay(500_000);
    }

    private void likeVideosOfNewAccounts(int numOfAccounts) {
        for (int i = 0; i <= numOfAccounts;) {
            int numberOfLikes = videoPage.getNumberOfLikes();
            boolean alreadyLiked = videoPage.isLikeClicked();
            System.out.println("Number of likes on video: " + numberOfLikes);

            if (alreadyLiked) {
                System.out.println("Already liked.");
            }

            boolean shouldProcess = numberOfLikes < MAX_LIKES_TO_PROCESS_PROFILE && !alreadyLiked;

            if (shouldProcess) {
                System.out.println("Processing profile #" + i);
                videoPage.clickLikeButton();
                videoPage.clickProfileButton();
                delayRandom(1000);
                int numberOfVideos = profilePage.getNumberOfVideosOnScreen();
                System.out.println("Found " + numberOfVideos + " videos");
                if (numberOfVideos > 0) {
                    profilePage.clickOnFirstVideo();
                    for (int j = 0; j < Math.min(numberOfVideos, MAX_VIDEOS_PER_PROFILE); j++) {
                        videoPage.likeVideoAndProceed();
                        delayRandom(200);
                    }
                    videoPage.goBack();
                }
                profilePage.goBack();
                i++;
            }

            System.out.println("Proceeding to next profile");
            videoPage.scroll(DefaultPage.DIRECTION_DOWN);
            videoPage.clickScreenCenter();
            delayRandom(200);
        }
    }
}
