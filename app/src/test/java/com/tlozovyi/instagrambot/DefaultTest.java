package com.tlozovyi.instagrambot;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public abstract class DefaultTest {
    private static final String PATH_TO_NODE = "/usr/local/bin/node";
    private static final String PATH_TO_APPIUM = "/usr/local/bin/appium";
    protected static final String SERVER_IP = "127.0.0.1";
    protected static final int SERVER_PORT = 4723;

    private static AppiumDriverLocalService service = null;

    protected static AndroidDriver driver;

    protected static void writeLog(String message) {
        AdbController.writeLog(message);
    }

    protected void delay(int millis) {
        AdbController.delay(millis);
    }

    protected void delayRandom(int millis) {
        int delay = (int) (millis * (1 + new Random().nextDouble()));
        delay(delay);
    }

    protected static void stopAppiumServer() {
        writeLog("Stopping Appium server");
        service.stop();
    }

    protected static void startAppiumServer() {
        writeLog("Starting Appium server");
        service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
                .usingDriverExecutable(new File(PATH_TO_NODE))
                .withAppiumJS(new File(PATH_TO_APPIUM))
                .withIPAddress(SERVER_IP)
                .usingPort(SERVER_PORT)
                .withArgument(GeneralServerFlag.LOG_LEVEL, "error"));
        service.start();
    }

    protected static void setupDriver(DesiredCapabilities capabilities) {
        URL serverAddress = null;
        try {
            serverAddress = new URL("http://" + SERVER_IP + ":" + SERVER_PORT + "/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver = new AndroidDriver(serverAddress, capabilities);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);

    }

    protected static DesiredCapabilities getBasicCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.TAKES_SCREENSHOT, "true");
        capabilities.setCapability("noReset","true");
        capabilities.setCapability("fullReset","false");
        return capabilities;
    }
}
